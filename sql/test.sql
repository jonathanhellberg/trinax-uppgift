DROP DATABASE IF EXISTS `test`;
CREATE DATABASE `test`;

use `test`;

drop table if exists `pictures`;
create table `pictures` (
    id int not null auto_increment,
    report_id int not null,
    image_name text not null,
    primary key (id)
);