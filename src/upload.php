<?php
require_once('db/db.php');
require_once('API/api.php');

header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

$postURL = "https://arbetsprov.trinax.se/api/v1/timereport";

$data = true;


if (isset($_POST['text']) && isset($_POST['workplace']) && isset($_POST['hours']) && isset($_POST['date'])) {

    $file = $_FILES['file'];

    $info = htmlentities($_POST['text'], ENT_QUOTES, 'UTF-8');
    $workplace_id = htmlentities($_POST['workplace'], ENT_QUOTES, 'UTF-8');
    $hours = htmlentities($_POST['hours'], ENT_QUOTES, 'UTF-8');
    $date = htmlentities($_POST['date'], ENT_QUOTES, 'UTF-8');

    $api = new API();

    $postData = (object) array(
        'info' => $info,
        'workplace_id' => $workplace_id,
        'hours' => $hours,
        'date' => $date
    );

    $response = $api->post($postURL, $postData);

    uploadFileToServer($file);

    insertPost($response['id'], $file);

    echo json_encode('{"status":"ok"}');
}

function insertPost($id, $file)
{
    $db = new db();

    $db->connect();
    $db->insert([$id, $file['name']]);
}

function uploadFileToServer($file)
{
    $location = "upload/" . $file['name'];

    if (move_uploaded_file($file['tmp_name'], $location)) {
        
    } else {
        
    }
}

