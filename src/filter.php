<?php
class Filter
{
    function mergeColumnsInObjects($object_one, $object_two)
    {

        for ($i = 0; $i < count($object_one); $i++) {
            foreach ($object_two as $value) {
                if ($value["id"] === $object_one[$i]["workplace_id"]) {
                    $object_one[$i]["workplace"] = $value["name"];
                }
            }
        }

        return $object_one;
    }
}
