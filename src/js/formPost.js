async function uploadFile(e) {
    e.preventDefault();
    let formData = new FormData();
  
    const text = document.getElementById("text").value;
    const date = document.getElementById("date").value;
    const hours = document.getElementById("hours").value;
    const workplace = document.getElementById("workplace").value;
  
    formData.append("file", fileupload.files[0]);
    formData.append("text", text);
    formData.append("date", date);
    formData.append("hours", hours);
    formData.append("workplace", workplace);
    
    await fetch('/upload.php', {
      method: "POST",
      body: formData
    })
    .then(response => response.json() )
    .then(result => { 
      const json = JSON.parse(result);
      if(json.status == "ok") {
        location.reload();
      }
     });

    console.log('The file has been uploaded successfully.');
}