<?php
class API
{

    public function get($href, $params = [])
    {

        $url = $href;

        $url = $this->handleParams($url, $params);

        $curl = curl_init();

        $header = array(
            'Accept: application/json',
            'Authorization: bearer d0d87abed8bf0ce06234367697eca8ca',
        );

        curl_setopt_array($curl, array(
            CURLOPT_HEADER => 0,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => $header,
        ));

        $response = curl_exec($curl);

        if ($response === false) {
       
        }

        curl_close($curl);

        $response = json_decode($response, true);

        return $response;
    }

    public function post($href, $params = [])
    {

        $payload = json_encode($params);
        $curl = curl_init();

        $header = array(
            'Accept: application/json',
            'Authorization: bearer d0d87abed8bf0ce06234367697eca8ca',
            'Content-Type: application/json',
        );

        curl_setopt_array($curl, array(
            CURLOPT_HEADER => 0,
            CURLOPT_URL => $href,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_POSTFIELDS => $payload,
        ));

        $response = curl_exec($curl);

        if ($response === false) {
          
        }

        curl_close($curl);

        $response = json_decode($response, true);

        return $response;
    }

    public function handleParams($href, $params)
    {
        $url = $href;

        if (!empty($params)) {
            $url .= "?";
            for ($i = 0; $i < count($params); $i++) {
                $url .= $params[$i];
                if (count($params) > ($i + 1)) {
                    $url .= "&";
                }
            }
        }
        return $url;
    }
}
