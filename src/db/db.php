<?php
class db
{

  private $servername = "db";
  private $username = "user";
  private $password = "test";

  public $conn;

  function connect()
  {
    try {
      $this->conn = new PDO("mysql:host=$this->servername;dbname=test", $this->username, $this->password);
      // set the PDO error mode to exception
      $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    } catch (PDOException $e) {

    }
  }

  function insert($data)
  {
    $sql = "INSERT INTO pictures (report_id, image_name) VALUES (?, ?)";
    $stmt = $this->conn->prepare($sql);
    $stmt->execute($data);
  }
}
