<?php
require_once('API/api.php');
require_once('filter.php');

$params = [];
$reportURL = "https://arbetsprov.trinax.se/api/v1/timereport";
$workplaceURL = "https://arbetsprov.trinax.se/api/v1/workplace";

if (isset($_POST['dateFrom'])) {
    $dateFromConverted = date('Y-m-d', strtotime($_POST['dateFrom']));
    $params[] = "from_date=" . $dateFromConverted;
}

if (isset($_POST['dateTo'])) {
    $dateToConverted = date('Y-m-d', strtotime($_POST['dateTo']));
    $params[] = "to_date=" . $dateToConverted;
}

if (isset($_POST['workplace_id'])) {
    $workplace_id = $_POST['workplace_id'];
    $params[] = "workplace=" . $workplace_id;
}

$api = new API();

$timeReports = $api->get($reportURL, $params);
$workplace = $api->get($workplaceURL);

$filters = new Filter();
$tableData = $filters->mergeColumnsInObjects($timeReports, $workplace);

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div>
        <form name="DateFilter" method="POST">
            <span>Från:</span>
            <input type="date" name="dateFrom" value="<?= ['dateFrom'] ?>" />
            <span>Till:</span>
            <input type="date" name="dateTo" value="<?= ['dateTo'] ?>" />
            <select name="workplace_id">
                <?php foreach ($workplace as $value) {  ?>
                    <option value="<?= $value['id']; ?>">
                        <?= $value['name']; ?>
                    </option>
                <?php }  ?>
            </select>
            <input type="submit" name="Submit" value="Filter">
        </form>
        </br>
        <table>
            <tr>
                <th>Date</th>
                <th>Workplace</th>
                <th>hours</th>
            </tr>
            <?php foreach ($tableData as $value) {  ?>
                <tr>
                    <td><?= $value['date']; ?></td>
                    <td><?= $value['workplace']; ?></td>
                    <td><?= $value['hours']; ?></td>
                </tr>
            <?php }  ?>
        </table>

        <h2>
            Add Post
        </h2>
        <form method="POST">
            <p>Datum:</p>
            <input id="date" type="date" name="date" value="<?= ['date'] ?>" />
            <p>Hours:</p>
            <input id="hours" type="number" name="hours" />
            </br>
            <p>Workplace:</p>
            <select id="workplace" name="workplace">
                <?php foreach ($workplace as $value) {  ?>
                    <option value="<?= $value['id']; ?>">
                        <?= $value['name']; ?>
                    </option>
                <?php }  ?>
            </select>
            </br>
            <p>Info:</p>
            <textarea id="text" name="text" rows="4" cols="50"></textarea>
            </br>
            </br>
            <input id="fileupload" type="file" name="fileupload" />
            </br>
            </br>
            <button id="upload-button" onclick="uploadFile(event)"> Upload </button>
        </form>
    </div>

    <script src="js/formPost.js"></script>
</body>

</html>