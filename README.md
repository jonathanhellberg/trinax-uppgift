## Install Required:
* [Docker](https://docs.docker.com/get-docker/)
* [Docker-Compose](https://docs.docker.com/compose/install/)
* [Git](https://github.com/git-guides/install-git)

## Install:

##### HTTP:
`git clone https://gitlab.com/jonathanhellberg/trinax-uppgift.git`

##### SSH:
`git clone git@gitlab.com:jonathanhellberg/trinax-uppgift.git`

`cd ./trinax-uppgift`

## Run applications:
`docker-compose up --build`

### Alternativ run:
`docker-compose up -d --build`

running in the background.

### Shutdown Application:

`docker-compose down`

###  Recreate/purge database

`docker-compose down`

`docker volume prune`

###  Enter/Check Database

`docker exec -it trinax-uppgift_db_1 bash`

`mysql -u user -p`

`test` "mysql password"

`use test;`

`SELECT * FROM pictures`

